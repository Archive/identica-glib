#ifndef __INIT__
#define __INIT__

RestProxy *init_service (void);
void dispose_service (RestProxy *init);
SoupSession *new_sync_session (void);
void dispose_sync_session (SoupSession *session);
SoupSession *new_async_session (void);
void dispose_async_session (SoupSession *session); 
struct id_credentials init_auth_data (const gchar* name, const gchar* password);

#endif /* __INIT__ */
