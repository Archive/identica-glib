#include <glib.h>
#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include "parse.h"

guint
get_user_id_sync(RestProxy *proxy,const gchar* screen_name) {
	  
	  GError** error;
	  RestProxyCall *call;
	  JsonNode *node;
	  JsonObject *object;
	  
	  call = rest_proxy_new_call(proxy);	      
      rest_proxy_call_set_function (call, "users/show.json");
      rest_proxy_call_add_param(call,"screen_name",screen_name);
            
      rest_proxy_call_sync(call,error);
      
      node = _make_json_node_from_call(call);
	
	if (!node)
    		return;
	
	object = json_node_get_object (node);
	
    g_object_unref(call);
    
    return json_object_get_int_member(object,"id"); 
}

const gchar*
get_user_realname_sync(RestProxy *proxy,const gchar* screen_name) {
	  
	  GError** error;
	  RestProxyCall *call;
	  JsonNode *node;
	  JsonObject *object;
	  
	  call = rest_proxy_new_call(proxy);	      
      rest_proxy_call_set_function (call, "users/show.json");
      rest_proxy_call_add_param(call,"screen_name",screen_name);
            
      rest_proxy_call_sync(call,error);
      
      node = _make_json_node_from_call(call);
	
	if (!node)
    		return;
	
	object = json_node_get_object (node);
	
    g_object_unref(call);
    
    return json_object_get_string_member(object,"name"); 
}

const gchar*
get_user_location_sync(RestProxy *proxy,const gchar* screen_name) {
	  
	  GError** error;
	  RestProxyCall *call;
	  JsonNode *node;
	  JsonObject *object;
	  
	  call = rest_proxy_new_call(proxy);	      
      rest_proxy_call_set_function (call, "users/show.json");
      rest_proxy_call_add_param(call,"screen_name",screen_name);
            
      rest_proxy_call_sync(call,error);
      
      node = _make_json_node_from_call(call);
	
	if (!node)
    		return;
	
	object = json_node_get_object (node);
	
    g_object_unref(call);
    
    return json_object_get_string_member(object,"location"); 
}
