#ifndef __AUTH__ 
#define __AUTH__

struct id_credentials {
	const gchar *username;
	const gchar *password;
};

void 
authentication_handler (SoupSession *session,
                 SoupMessage *msg,
                 SoupAuth    *auth,
                 gboolean     retrying,
                 gpointer     user_data);
                 
void
verify_credentials(SoupSession *session, struct id_credentials *id_cred);                 

#endif /* __AUTH__ */
