#ifndef __USER__
#define __USER__

guint get_user_id_sync(RestProxy *proxy,const gchar* screen_name);
const gchar* get_user_realname_sync(RestProxy *proxy,const gchar* screen_name);
const gchar* get_user_location_sync(RestProxy *proxy,const gchar* screen_name);

#endif /* __USER__ */
