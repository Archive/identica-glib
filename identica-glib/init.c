#include <glib.h>
#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include "auth.h"

RestProxy 
*init_service (void) 
{
     RestProxy *proxy;
     proxy = (RestProxy *) rest_proxy_new ("http://identi.ca/api/", FALSE);
	 return proxy;
}

void
*dispose_service (RestProxy *proxy)
{
	g_object_unref(proxy);
}

SoupSession
*new_sync_session (void)
{
	SoupSession *sync_session;
	sync_session = soup_session_sync_new();
	return sync_session;
}

SoupSession
*new_async_session (void)
{
	SoupSession *async_session;
	async_session = soup_session_async_new();
	return async_session;
}

void
dispose_sync_session (SoupSession *session)
{
	g_object_unref(session);
} 

void 
dispose_async_session (SoupSession *session)
{
	g_object_unref(session);
}

struct id_credentials
init_auth_data (const gchar* name, const gchar* password)
{
	struct id_credentials id_cred;
	
	(&id_cred)->username = name;
	(&id_cred)->password = password;

	return id_cred;
}
