#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

RestXmlNode *
_make_node_from_call (RestProxyCall *call)
{
  static RestXmlParser *parser = NULL;
  RestXmlNode *root;
  const gchar *payload;
  goffset len;

  if (call == NULL)
    return NULL;
	
  if (parser == NULL)
    parser = rest_xml_parser_new ();

  if (!SOUP_STATUS_IS_SUCCESSFUL (rest_proxy_call_get_status_code (call))) {
    g_warning (G_STRLOC ": Error from Service: %s (%d)",
               rest_proxy_call_get_status_message (call),
               rest_proxy_call_get_status_code (call));
    return NULL;
  } 

  payload = rest_proxy_call_get_payload (call);
  len = rest_proxy_call_get_payload_length (call);
  
  root = rest_xml_parser_parse_from_data (parser,
                                          payload,
                                          len);

  if (root == NULL) {
    g_warning (G_STRLOC ": Error parsing payload from Service: %s",
               rest_proxy_call_get_payload (call));
    return NULL;
  }

  return root;
}

JsonNode *
_make_json_node_from_call (RestProxyCall *call)
{
  static JsonParser *parser = NULL;
  JsonNode *root;
  const gchar *payload;
  goffset len;
  gboolean ret;
  GError *error = NULL;

  if (call == NULL)
    return NULL;

  if (parser == NULL)
    parser = json_parser_new();

  if (!SOUP_STATUS_IS_SUCCESSFUL (rest_proxy_call_get_status_code (call))) {
    g_warning (G_STRLOC ": Error from Service: %s (%d)",
               rest_proxy_call_get_status_message (call),
               rest_proxy_call_get_status_code (call));
    return NULL;
  } 

  payload = rest_proxy_call_get_payload (call);
  len = rest_proxy_call_get_payload_length (call);
  
  ret = json_parser_load_from_data(parser,payload,len,&error);

  if (ret == FALSE) {
    g_warning (G_STRLOC ": Error parsing payload from Service: %s",
               rest_proxy_call_get_payload (call));
    return NULL;
  }
 
  return json_parser_get_root(parser);
}
