#ifndef __TIMELINE__
#define __TIMELINE__

GList* get_home_timeline_sync (SoupSession *session, struct id_credentials *id_cred);
GList* get_mentions_sync (SoupSession *session, struct id_credentials *id_cred);
GList* get_auth_user_timeline_sync (SoupSession *session, struct id_credentials *id_cred);
GList* get_user_timeline_sync (SoupSession *session, const gchar *screen_name);

#endif /* __TIMELINE__ */
