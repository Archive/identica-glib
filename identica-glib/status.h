#ifndef __STATUS__
#define __STATUS__

void update_status(SoupSession *session, struct id_credentials *id_cred, const gchar *status);

#endif /* __STATUS__ */
