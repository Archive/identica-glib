#include <glib.h>
#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include "auth.h"
#include "parse.h"

GList*
get_home_timeline_sync (SoupSession *session, struct id_credentials *id_cred)
{

	GError **error;
	RestProxyCall *call;
	gint count;
	GList *list = NULL;

	SoupMessage *msg;
	const gchar* uri; 
	const gchar* payload;
	
	uri = "https://identi.ca/api/statuses/home_timeline.xml";
	
	g_signal_connect(session, "authenticate", G_CALLBACK(authentication_handler), (gpointer) id_cred);

	msg = soup_message_new ("POST", uri);
	soup_session_send_message (session,msg);
	
	RestXmlParser *parser = rest_xml_parser_new ();
	RestXmlNode *child;
	
	RestXmlNode *node = rest_xml_parser_parse_from_data (parser,
                                          msg->response_body->data,
                                          msg->response_body->length);
                                          
 	if (!node)
    		return;											

	for (child = rest_xml_node_find (node, "status"), count = 0; child && count < 10; child = child->next, count++) {
		RestXmlNode *name = rest_xml_node_find(child,"screen_name");
		RestXmlNode *text = rest_xml_node_find(child,"text");
		list = g_list_append(list,name->content);
		list = g_list_append(list,text->content);
	}

	return list;
}

GList*
get_mentions_sync (SoupSession *session, struct id_credentials *id_cred)
{

	GError **error;
	RestProxyCall *call;
	gint count;
	GList *list = NULL;

	SoupMessage *msg;
	const gchar* uri; 
	const gchar* payload;
	
	uri = "https://identi.ca/api/statuses/mentions.xml";
	
	g_signal_connect(session, "authenticate", G_CALLBACK(authentication_handler), (gpointer) id_cred);

	msg = soup_message_new ("POST", uri);
	soup_session_send_message (session,msg);
	
	RestXmlParser *parser = rest_xml_parser_new ();
	RestXmlNode *child;
	
	RestXmlNode *node = rest_xml_parser_parse_from_data (parser,
                                          msg->response_body->data,
                                          msg->response_body->length);
                                          
 	if (!node)
    		return;											

	for (child = rest_xml_node_find (node, "status"); child; child = child->next) {
		RestXmlNode *name = rest_xml_node_find(child,"screen_name");
		RestXmlNode *text = rest_xml_node_find(child,"text");
		list = g_list_append(list,name->content);
		list = g_list_append(list,text->content);
	}

	return list;
}

GList*
get_auth_user_timeline_sync (SoupSession *session, struct id_credentials *id_cred)
{

	GError **error;
	RestProxyCall *call;
	gint count;
	GList *list = NULL;

	SoupMessage *msg;
	const gchar* uri; 
	const gchar* payload;
	
	uri = "https://identi.ca/api/statuses/user_timeline.xml";
	
	g_signal_connect(session, "authenticate", G_CALLBACK(authentication_handler), (gpointer) id_cred);

	msg = soup_message_new ("GET", uri);
	soup_session_send_message (session,msg);
	
	RestXmlParser *parser = rest_xml_parser_new ();
	RestXmlNode *child;
	
	RestXmlNode *node = rest_xml_parser_parse_from_data (parser,
                                          msg->response_body->data,
                                          msg->response_body->length);
                                          
 	if (!node)
    		return;											

	for (child = rest_xml_node_find (node, "status"); child; child = child->next) {
		RestXmlNode *name = rest_xml_node_find(child,"screen_name");
		RestXmlNode *text = rest_xml_node_find(child,"text");
		list = g_list_append(list,name->content);
		list = g_list_append(list,text->content);
	}

	return list;
}


GList*
get_user_timeline_sync (SoupSession *session, const gchar *screen_name)
{

	GError **error;
	RestProxyCall *call;
	GList *list = NULL;

	SoupMessage *msg;
	const gchar* uri; 
	const gchar* payload;
	
	uri = "https://identi.ca/api/statuses/user_timeline.xml?screen_name=";
	
	payload = g_strconcat(uri,screen_name,NULL); 
	
	msg = soup_message_new ("GET", payload);
	soup_session_send_message (session,msg);
	
	RestXmlParser *parser = rest_xml_parser_new ();
	RestXmlNode *child;
	
	RestXmlNode *node = rest_xml_parser_parse_from_data (parser,
                                          msg->response_body->data,
                                          msg->response_body->length);
                                          
 	if (!node)
    		return;											

	for (child = rest_xml_node_find (node, "status"); child; child = child->next) {
		RestXmlNode *name = rest_xml_node_find(child,"screen_name");
		RestXmlNode *text = rest_xml_node_find(child,"text");
		list = g_list_append(list,name->content);
		list = g_list_append(list,text->content);
	}

	return list;
}

GList* 
get_public_timeline_sync (RestProxy *proxy) {
	
	GError **error;
	RestProxyCall *call;
	RestXmlNode *node;
	RestXmlNode *child;
	gint count;
	GList *list = NULL;


	call = rest_proxy_new_call(proxy);	      
    rest_proxy_call_set_function (call, "statuses/public_timeline.xml");
    rest_proxy_call_sync(call, error);
      
    node = _make_node_from_call(call);

 	if (!node)
    		return;

	for (child = rest_xml_node_find (node, "status"), count = 0; child && count < 10; child = child->next, count++) {
		RestXmlNode *name = rest_xml_node_find(child,"screen_name");
		RestXmlNode *text = rest_xml_node_find(child,"text");
		list = g_list_append(list, name->content);
		list = g_list_append(list, text->content);
	}
	
	g_object_unref(call);
    return list;
}

