#include <glib.h>
#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include <identica-glib/auth.h>
#include <identica-glib/init.h>

int 
main (void) 
{
	GMainLoop *loop;
	SoupSession *sess_async;
	struct id_credentials id_cred;
	
	g_type_init();

    loop = g_main_loop_new (NULL, FALSE);
   
	RestProxy* proxy =  init_service();
   
	sess_async = soup_session_async_new();
	id_cred = init_auth_data("username","password");
		
	verify_credentials(sess_async,&id_cred);
	update_status(sess_async,&id_cred,"everythin' works fine");

    g_main_loop_run(loop);      

	dispose_async_session(sess_async);
	dispose_service(proxy);
	
	g_main_loop_unref(loop);
	return 0;
}

