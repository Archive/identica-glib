#include <glib.h>
#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include <identica-glib/auth.h>
#include <identica-glib/init.h>

int 
main (void) 
{
	GMainLoop *loop;
	SoupSession *sess_sync;
	GList *list;
	struct id_credentials id_cred;

	g_type_init();

    loop = g_main_loop_new (NULL, FALSE);
	sess_sync = soup_session_sync_new();
	id_cred = init_auth_data("username","password");

	list = (GList *) get_home_timeline_sync(sess_sync,&id_cred);
	
	while( list ) {
			const gchar *payload = list->data;
			g_print("%s\n",payload);
			list = list->next;
	}

	list = (GList *) get_mentions_sync(sess_sync,&id_cred);
	
	while( list ) {
			const gchar *payload = list->data;
			g_print("%s\n",payload);
			list = list->next;
	}

	g_main_loop_run(loop);
	g_object_unref(list);
	dispose_sync_session(sess_sync);
	
	g_main_loop_unref(loop);
	
	return 0;

}
