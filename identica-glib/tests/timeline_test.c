#include <glib.h>
#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include <identica-glib/auth.h>
#include <identica-glib/init.h>

int 
main (void) 
{
	GMainLoop *loop;
	RestProxy* proxy;

	g_type_init();
	loop = g_main_loop_new (NULL, FALSE);
   
	proxy =  init_service();
   
 	g_print(" %d \n", get_user_id_sync(proxy,"eepica"));
	g_print(" %s \n", get_user_location_sync(proxy,"eepica"));
	g_print(" %s \n", get_user_realname_sync(proxy,"eepica"));

	g_main_loop_run(loop);
	dispose_service(proxy);
	
	g_main_loop_unref(loop);
	
	return 0;
}
