#include <glib.h>
#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include <identica-glib/auth.h>
#include <identica-glib/init.h>

int 
main (void) 
{
	GMainLoop *loop;
	SoupSession *sess_sync;
	GList *list;
	
	g_type_init();

	sess_sync = soup_session_sync_new();
	loop = g_main_loop_new (NULL, FALSE);
	
	list = (GList *) get_user_timeline_sync(sess_sync, "eepica");
	
	while( list ) {
			const gchar *payload = list->data;
			g_print(" %s\n ",payload);
			list = list->next;
	}
    
    g_main_loop_run(loop);      

	dispose_sync_session(sess_sync);
	
	g_main_loop_unref(loop);

	return 0;
	
}
