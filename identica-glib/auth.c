#include <glib.h>
#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include "auth.h"

void
verify_credentials_cb (SoupSession *session, SoupMessage *msg, gpointer user_data)
{
	RestXmlParser *parser = NULL;
	const gchar *payload;
	goffset len;
	RestXmlNode *root;
   			
  if (parser == NULL)
    parser = rest_xml_parser_new ();

  if (msg->status_code != 200) {
    g_warning (G_STRLOC ": Error from Service: %s (%d)",
               msg->reason_phrase,
               msg->status_code);
    return;
  } 

  payload = msg->response_body->data;
  len = msg->response_body->length;
  
  root = rest_xml_parser_parse_from_data (parser,
                                          payload,
                                          len);

  if (root == NULL) {
    g_warning (G_STRLOC ": Error parsing payload from Service \n");
    return;
  }
	return;
}

void
verify_credentials(SoupSession *session, struct id_credentials *id_cred) {
	
	SoupMessage *msg;
	
	msg = soup_message_new ("GET", "https://identi.ca/api/account/verify_credentials.xml");
	soup_session_queue_message (session, msg, verify_credentials_cb, NULL);
	
	g_signal_connect(session, "authenticate", G_CALLBACK(authentication_handler), (gpointer) (id_cred));
}

void 
authentication_handler (SoupSession *session,
                 SoupMessage *msg,
                 SoupAuth    *auth,
                 gboolean     retrying,
                 gpointer     user_data)
{	
	struct id_credentials *id_cred;
	
	g_message("Authentication CALLBACK activated \n");
	
	id_cred = user_data;
	
	soup_auth_authenticate (auth,id_cred->username,id_cred->password);
}

