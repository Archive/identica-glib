#ifndef __PARSE__ 
#define __PARSE__

JsonNode * _make_json_node_from_call (RestProxyCall *call);
RestXmlNode * _make_node_from_call (RestProxyCall *call);

#endif /* __PARSE__ */
