/*

void
get_user_id_cb 
(RestProxyCall *call, const GError *error, GObject *weak_object, gpointer userdata) 
{
	JsonNode *node;
	JsonNode *child;
  	JsonObject *object;
 
	node = _make_json_node_from_call(call);
	
	if (!node)
    		return;
	
	object = json_node_get_object (node);
	
	#ifdef DEBUG
	g_print("%s \n",json_object_get_string_member(object,"name"));
	#endif
}

void 
get_user_id_async(RestProxy *proxy, const gchar* screen_name)
{
	  GError** error;
	  RestProxyCall *call;
	  JsonNode *node;
	  JsonObject *object;
	  
	  call = rest_proxy_new_call(proxy);	      
      rest_proxy_call_set_function (call, "users/show.json");
      rest_proxy_call_add_param(call,"screen_name",screen_name);
      
      rest_proxy_call_async(call, get_user_id_cb, NULL, NULL, NULL);
	
	g_object_unref(call);
}

*/

/*

void 
get_public_timeline_cb
(RestProxyCall *call, const GError *error, GObject *weak_object, gpointer userdata) 
{
	RestXmlNode *node;
	RestXmlNode *child;
	gint count;

	node = _make_node_from_call(call);

 	if (!node)
    		return;

	for (child = rest_xml_node_find (node, "status"), count = 0; child && count < 10; child = child->next, count++) {
		RestXmlNode *name = rest_xml_node_find(child,"screen_name");
		RestXmlNode *text = rest_xml_node_find(child,"text");
		#ifdef DEBUG
		g_print("%s \n",name->content);
		g_print("%s \n",text->content);	
		g_print(" --------- \n");
		#endif
	}
	g_object_unref(call);
}

void 
get_public_timeline_async(RestProxy *proxy) {
	
      RestProxyCall *call = rest_proxy_new_call(proxy);	      
      rest_proxy_call_set_function (call, "statuses/public_timeline.xml");
      rest_proxy_call_async(call, get_public_timeline_cb, NULL, NULL, NULL);
      
}

*/
