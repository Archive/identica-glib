#include <rest/rest-proxy.h>
#include <rest/rest-xml-parser.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include "auth.h"

void
update_cb (SoupSession *session, SoupMessage *msg, gpointer user_data)
{
	g_print("This is the response code %d \n", msg->status_code);
	g_print("This is the request result %s \n", msg->response_body->data);
}

void
update_status_cb (SoupSession *session, SoupMessage *msg, gpointer user_data)
{
	g_message("Update status CALLBACK called"); 
}

void
update_status(SoupSession *session, struct id_credentials *id_cred, const gchar *status)
{
	SoupMessage *msg;
	const gchar* uri; 
	const gchar* payload;
	
	uri = "https://identi.ca/api/statuses/update.xml?status=";
	
	payload = g_strconcat(uri,status,NULL);
		
	msg = soup_message_new ("POST", payload);
	soup_session_queue_message (session,msg,update_status_cb, NULL);
	
	g_signal_connect(session, "authenticate", G_CALLBACK(authentication_handler), (gpointer) id_cred);
	
}
